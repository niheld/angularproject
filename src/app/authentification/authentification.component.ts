import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from  '@angular/router';
import { Authentif } from '../classes/authentif';
import { AuthentifService } from '../authentif.service';
import { Login } from '../classes/login';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.scss']
})
export class AuthentificationComponent implements OnInit {

  authentif = new Authentif;
  /*loginForm: FormGroup;
  isSubmitted  =  false;*/

  constructor(private authentifService: AuthentifService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {

    /*this.loginForm  =  this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });*/

  }

  //get formControls() { return this.loginForm.controls; }

  login(){
    /*console.log(this.loginForm.value);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return;
    }*/
    //console.log(this.loginForm.value);
    //this.authentifService.isLoggedIn(this.loginForm.value);
    let respLogin = new Login;

    this.authentifService.login(this.authentif).subscribe(
      response => {
        respLogin.token = response.token;
        this.router.navigateByUrl('/home');
        localStorage.setItem('token', respLogin.token);

      }, error => {
        console.log('error');
      }
    );

  }

  
}
