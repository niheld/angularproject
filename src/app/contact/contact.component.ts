import { Component, OnInit } from '@angular/core';
import { Contact } from '../classes/contact';
import { ContactService } from '../contact.service';
import { concat } from 'rxjs/internal/observable/concat';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contact = new Contact;
  constructor(private contactService : ContactService) { }

  ngOnInit() {
  }
  
  postContact(){

    console.log(this.contact);
        
    this.contactService.postContactData(this.contact).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log('error');
      }
    );

  }

}
