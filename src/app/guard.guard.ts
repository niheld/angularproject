import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentifService } from './authentif.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {

  constructor(private authentif: AuthentifService, private router: Router){}

  canActivate() : boolean{
    if(this.authentif.isLogeddIn()){
      return true;
    }else {
      this.router.navigateByUrl('/connexion');
    }
  }
    /*next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;*/
  
  
}
