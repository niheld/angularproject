import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from 'src/app/home/home.component';
import { HotelsComponent } from 'src/app/hotels/hotels.component';
import { VoyagesComponent } from 'src/app/voyages/voyages.component';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';
import { DetailshotelComponent } from 'src/app/detailshotel/detailshotel.component';
import { AuthentificationComponent } from 'src/app/authentification/authentification.component';
import { ClientindivComponent } from 'src/app/clientindiv/clientindiv.component';
import { GuardGuard } from './guard.guard';



const routes: Routes = [ 
  { path: "" , component: HomeComponent},
  { path: "home" , component: HomeComponent},
  { path: "hotels" , component: HotelsComponent},
  { path: "voyages" , component: VoyagesComponent},
  { path: "contact" , component: ContactComponent},
  { path: "details_hotel", component: DetailshotelComponent},
  { path: "connexion", component:  AuthentificationComponent},
  { path: "espaceclient", 
    component: ClientindivComponent,
  canActivate : [GuardGuard]},
  { path:'**', component: NotFoundComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
