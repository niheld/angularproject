import { Component, OnInit } from '@angular/core';
import { AuthentifService } from '../authentif.service';

let logedin : string;

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(private authentifService : AuthentifService) { }

  ngOnInit() {

  }

  logout(){


    let token = localStorage.getItem('token');
    this.authentifService.logout(token);
  }



}
