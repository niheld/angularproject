import { Injectable } from '@angular/core';
import { Authentif } from './classes/authentif';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Login } from './classes/login';
import { Router } from  '@angular/router';

const url = "http://myfreedobt.cluster011.ovh.net/api/login_check";
const httpOptions = {
  headers : new HttpHeaders({'Content-Type' : 'application/json'}),
};

@Injectable({
  providedIn: 'root'
})

export class AuthentifService {

  constructor(private http: HttpClient, private router: Router) {}

  login(authentif: Authentif): Observable <Login>{

    return this.http.post<Login>(url, authentif, httpOptions);

  }

  isLogeddIn(): boolean{

    return (!!localStorage.getItem('token'));
      
  }

  isLogeddOut(): boolean{

    return (!localStorage.getItem('token'));
      
  }

  logout(token : string){

    if(token == localStorage.getItem('token')){

      localStorage.removeItem('token');
      this.router.navigateByUrl('/connexion');

    }   
  }

  getToken(): string{
    return (localStorage.getItem('token'));
  }


}
