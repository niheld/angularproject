import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from './classes/contact';
import { Observable } from 'rxjs/internal/Observable';

const url = "https://www.freedomtravel.tn/ng/addcontactform.php";
const httpOptions = {
  headers : new HttpHeaders({'Content-Type' : 'application/json'}),
};

@Injectable({
  providedIn: 'root'
})

export class ContactService {

  constructor(private http : HttpClient) { }

  postContactData(contact : Contact): Observable <Contact>{
    
    return this.http.post<Contact>(url, contact, httpOptions);

  }
}
