import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpEvent, HttpRequest } from '@angular/common/http';
import { AuthentifService } from 'src/app/authentif.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  
  constructor(private authentif: AuthentifService){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent <any>> {
    
    req = req.clone({ setHeaders: { Authorization: `Bearer ${this.authentif.getToken()}`}});
    return next.handle(req);

  }

}
