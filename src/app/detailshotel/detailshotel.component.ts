import { Component, OnInit } from '@angular/core';  
import { CarouselhotelService } from '../carouselhotel.service';
import { Carouselhotel } from 'src/app/classes/carouselhotel';
import { Detailshotel } from '../classes/detailshotel';



@Component({
  selector: 'app-detailshotel',
  templateUrl: './detailshotel.component.html',
  styleUrls: ['./detailshotel.component.scss']
})
export class DetailshotelComponent implements OnInit {

  cars : any;
  carouselHotel = new Carouselhotel;
  detailshotel = new Detailshotel;

  constructor(private carousel : CarouselhotelService) { }

  ngOnInit() {
    this.carouselHotel.id = this.detailshotel.id;
    console.log(this.detailshotel.id);
    this.carousel.getCarouselHotel(this.carouselHotel).subscribe(
      res => {
        console.log(res);
        this.cars = res;
      },
      error => {

      }
    );
  }



}
