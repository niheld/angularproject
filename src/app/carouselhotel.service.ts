import { Injectable } from '@angular/core';
import { Carouselhotel } from './classes/carouselhotel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

const url = "https://www.freedomtravel.tn/ng/carouselHotel.php";

const HttpOptions = {
  headers : new HttpHeaders({'content-Type' : 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class CarouselhotelService {

  constructor(private http : HttpClient) { }

  getCarouselHotel(carousel : Carouselhotel): Observable <Carouselhotel>{
    

  return this.http.post<Carouselhotel>(url,carousel ,HttpOptions);
  }
}
