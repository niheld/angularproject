import { Injectable } from '@angular/core';
import { Hotel } from './classes/hotel';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';


const url = "https://www.freedomtravel.tn/ng/hotels.php";
const httpOptions = {
  headers : new HttpHeaders({'Content-Type' : 'application/json'}),
};


@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http : HttpClient) { }

  /*getHotels(){
    return this.http.get(url);
    
  }*/

  getHotels(hotel: Hotel){
    return this.http.post(url, hotel, httpOptions);
  }
  
}
