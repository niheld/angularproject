import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientindivComponent } from './clientindiv.component';

describe('ClientindivComponent', () => {
  let component: ClientindivComponent;
  let fixture: ComponentFixture<ClientindivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientindivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientindivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
