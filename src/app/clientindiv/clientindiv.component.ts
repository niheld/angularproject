import { Component, OnInit } from '@angular/core';
import { ClientindivService } from '../clientindiv.service';
import { Clientindiv } from '../classes/clientindiv';
import { error } from 'selenium-webdriver';

let clientindiv = new Clientindiv;

@Component({
  selector: 'app-clientindiv',
  templateUrl: './clientindiv.component.html',
  styleUrls: ['./clientindiv.component.scss']
})
export class ClientindivComponent implements OnInit {

  constructor(private client : ClientindivService ) { }

  ngOnInit() {

    this.client.getClientIndiv().subscribe(
      response => {

        console.log(response);

      }, error => {
        console.log('error');
      }
    );

  }

}
