import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { HotelsComponent } from './hotels/hotels.component';
import { SoireesComponent } from './soirees/soirees.component';
import { VoyagesComponent } from './voyages/voyages.component';
import { OmraComponent } from './omra/omra.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DetailshotelComponent } from './detailshotel/detailshotel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { CardhotelPipe } from './cardhotel.pipe';
import { AuthentificationComponent } from './authentification/authentification.component';
import { ClientindivComponent } from './clientindiv/clientindiv.component';
import { InterceptorService } from './interceptor.service';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';



@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent,
    ContactComponent,
    HomeComponent,
    HotelsComponent,
    SoireesComponent,
    VoyagesComponent,
    OmraComponent,
    NotFoundComponent,
    DetailshotelComponent,
    CardhotelPipe,
    AuthentificationComponent,
    ClientindivComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDaterangepickerMd.forRoot(),
    DateRangePickerModule 

  ],
  providers: [
    {provide: HTTP_INTERCEPTORS,
    useClass: InterceptorService,
    multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
