import { Component, OnInit } from '@angular/core';
import { VilleService } from '../ville.service';
import { HotelService } from '../hotel.service';
import { Carouselhotel } from 'src/app/classes/carouselhotel';
import { Hotel } from 'src/app/classes/hotel';
import { CardhotelPipe } from '../cardhotel.pipe';
import { Detailshotel } from '../classes/detailshotel';
import * as moment from 'moment';




@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})



export class HotelsComponent implements OnInit {
  
  now = moment().format('l');

  selected = {startDate: moment(), endDate:  moment(this.now).add('days', 1)};

  villes : any;
  hotels : any;
  htmlToAdd : string =  "";
  hotel = new Hotel;
  detailshotel = new Detailshotel;

public month: number = new Date().getMonth();
  public fullYear: number = new Date().getFullYear();
  public day: number = new Date().getDate();
  
  public start: Date = new Date(this.fullYear, this.month , this.day);
  public end: Date = new Date(this.fullYear, this.month, this.day + 1);
  public idH: number;

  constructor( private vil : VilleService, private hotelService: HotelService) { }

  ngOnInit() {
    
    this.vil.test().subscribe(
      res => {
        console.log(res);
        this.villes = res;
      },
      error => {
        console.log('error');
      }
    );

    this.hotel.check_in = this.start;
    this.hotel.check_out = this.end;

    this.hotelService.getHotels(this.hotel).subscribe(
      response => {

        console.log('response hotels');
        console.log(response);
        this.hotels = response;
      },
      error => {
        console.log('error');
      }
    );   
  }


  filterHotels(){
  
    
    this.hotel.check_in = this.start;
    this.hotel.check_out = this.end;
    console.log(this.hotel);
    this.hotelService.getHotels(this.hotel).subscribe(
      response => {

        console.log('response hotels');
        console.log(response);
        this.hotels = response;
      },
      error => {
        console.log('error');
      }
    );
    

  }

  setIdHotel( idH ){
    console.log(this.idH);
  }



}
