import { Injectable } from '@angular/core';
import { Clientindiv } from './classes/clientindiv';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthentifService } from './authentif.service';
import { Observable } from 'rxjs/internal/Observable';



let url = "http://myfreedobt.cluster011.ovh.net/api/client_indivs.json";

@Injectable({
  providedIn: 'root'
})
export class ClientindivService {

  constructor(private http : HttpClient, private authentif: AuthentifService) { }

  getClientIndiv(): Observable <Clientindiv>{
    let httpOptions = {
      headers : new HttpHeaders({'Content-Type' : 'application/json'}),
    };
    return this.http.get<Clientindiv>(url, httpOptions);
  }
}
