import { Injectable } from '@angular/core';
import { Ville } from './classes/ville';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';

const url = "https://www.freedomtravel.tn/ng/villes.php";
const httpOptions = {
  headers : new HttpHeaders({'Content-Type' : 'application/json'}),
};
@Injectable({
  providedIn: 'root'
})
export class VilleService {
  
  constructor(private http: HttpClient, private handler: HttpBackend) { 
    this.http = new HttpClient(handler);
  }

  test(){
    return this.http.get(url, httpOptions);
    
  }
}
