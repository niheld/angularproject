import { TestBed } from '@angular/core/testing';

import { CarouselhotelService } from './carouselhotel.service';

describe('CarouselhotelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarouselhotelService = TestBed.get(CarouselhotelService);
    expect(service).toBeTruthy();
  });
});
